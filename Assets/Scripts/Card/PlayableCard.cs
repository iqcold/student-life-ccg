﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using Scripts.Board;

namespace Scripts.Card
{
    public class PlayableCard : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler, IPointerEnterHandler,
        IPointerExitHandler, IPointerClickHandler
    {
        public static readonly float DefaultCardDepth = 0F;

        public Card CardInfo { get; set; }

        public Vector3 DefaultLocalPosition { get; set; }
        public Vector3 DefaultLocalRotation { get; set; }

        public CardHolder DefaultCardHolder;

        [Min(0.01F)] public float SpeedMultiplier;

        public bool IsHovered { get; private set; }
        public bool IsDragged { get; private set; }

        private Rigidbody _body;
        private RectTransform _rectTransform;

        private float _mousePosition;

        private Coroutines _coroutines;

        public void OnBeginDrag(PointerEventData eventData)
        {
            IsDragged = true;
        }

        public void OnDrag(PointerEventData eventData)
        {
            if (DefaultCardHolder.HolderType == CardHolder.HolderTypes.PlayerHand
                || DefaultCardHolder.HolderType == CardHolder.HolderTypes.EnemyHand)
            {

            }
        }

        public void OnEndDrag(PointerEventData eventData)
        {
            IsDragged = false;
        }

        public void OnPointerClick(PointerEventData eventData)
        {

        }

        public void OnPointerEnter(PointerEventData eventData)
        {
            IsHovered = true;
            if (DefaultCardHolder.HolderType == CardHolder.HolderTypes.PlayerHand
                || DefaultCardHolder.HolderType == CardHolder.HolderTypes.EnemyHand)
            {
                DefaultLocalPosition = new Vector3(transform.localPosition.x, transform.localPosition.y,
                    transform.localPosition.z);
                if (_coroutines.OnPointerExit != null)
                {
                    StopCoroutine(_coroutines.OnPointerExit);
                    _coroutines.OnPointerExit = null;
                }

                _coroutines.OnPointerEnter = StartCoroutine(LocalMoveTo(new Vector3(transform.localPosition.x,
                    Hand.LocalCardHeightOnHover, transform.localPosition.z)));
                //StartCoroutine(LocalRotate(new Vector3(0, 0, 65)));
            }
        }

        public void OnPointerExit(PointerEventData eventData)
        {
            if (!IsDragged)
            {
                IsHovered = false;
                if (_coroutines.OnPointerEnter != null)
                {
                    StopCoroutine(_coroutines.OnPointerEnter);
                    _coroutines.OnPointerEnter = null;
                }

                _coroutines.OnPointerExit = StartCoroutine(LocalMoveTo(DefaultLocalPosition));
            }
        }

        public void SetLocalPosition(Vector3 position)
        {
            DefaultLocalPosition = position;
            LocalMoveTo(position);
        }

        public void SetLocalRotation(Vector3 rotation)
        {
         // TODO: call LocalRotate
        }

        private IEnumerator LocalMoveTo(Vector3 position)
        {
            return LocalMoveTo(position, SpeedMultiplier);
        }

        private IEnumerator LocalMoveTo(Vector3 position, float speedMultiplier)
        {
            Debug.Log(position);
            do
            {
                _body.velocity = transform.TransformDirection(position - transform.localPosition) * speedMultiplier;
                yield return new WaitForFixedUpdate();
            } while (_body.velocity.sqrMagnitude > .00001F);

            _body.velocity = Vector3.zero;
            transform.localPosition = position;
            yield return null;
        }

        [Obsolete("Very large margin of error")]
        private IEnumerator LocalRotate(Vector3 rotation)
        {
            do
            {
                // TODO: Another way to rotate
                // margin of error
                _body.angularVelocity = transform.TransformDirection(rotation - transform.localRotation.eulerAngles);
                //Debug.Log(_body.angularVelocity.sqrMagnitude)
                yield return new WaitForFixedUpdate();
            } while (_body.angularVelocity.sqrMagnitude > 10F);

            _body.angularVelocity = Vector3.zero;
            transform.localRotation = Quaternion.Euler(rotation);
            yield return null;
        }



        private void Awake()
        {
            _body = GetComponent<Rigidbody>();
            _rectTransform = GetComponent<RectTransform>();
            _coroutines = new Coroutines();
        }

        void Start()
        {

        }

        private void FixedUpdate()
        {

        }

        void Update()
        {

        }

        private struct Coroutines
        {
            public Coroutine OnPointerEnter;
            public Coroutine OnPointerExit;
        }
    }
}