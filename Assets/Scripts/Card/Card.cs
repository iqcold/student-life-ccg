﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Scripts.Card
{
    public class Card : ScriptableObject
    {
        public string Name { get; set; }
        public int Cost { get; set; }
        public string About { get; set; }
        public Sprite Logo { get; set; }

        public Skill[] Skills { get; set; }
        // effects

        public Card()
        {
            Name = null;
            Cost = 0;
            About = null;
            Logo = null;
            Skills = null;
        }

        public Card(string name, int cost, string about, Sprite logo, Skill[] skills)
        {
            Name = name;
            Cost = cost;
            About = about;
            Logo = logo;
            Skills = skills;
        }

    }
}