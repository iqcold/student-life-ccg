﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Scripts.Card
{
    public abstract class Skill
    {
        public string Name { get; set; }
        public string About { get; set; }
        public Sprite Logo { get; set; }

        public abstract void Use();

        public abstract void Use(Card self);

        public abstract void UseOn(Card card);

    }
}