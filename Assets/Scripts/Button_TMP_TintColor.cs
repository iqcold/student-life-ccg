﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class Button_TMP_TintColor : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler, IPointerDownHandler, IPointerUpHandler
{
    public TMPro.TextMeshProUGUI TextMesh;
    private Button button;
    private Color defaultColor;

    public void OnPointerDown(PointerEventData eventData)
    {
        TextMesh.color = button.colors.pressedColor;
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        if (!eventData.dragging)
        {
            TextMesh.color = button.colors.highlightedColor;
        }
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        if (!eventData.pointerPress)
        {
            TextMesh.color = defaultColor;
        }
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        TextMesh.color = defaultColor;
    }

    // Start is called before the first frame update
    void Start()
    {
        defaultColor = TextMesh.color;
        button = GetComponent<Button>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
