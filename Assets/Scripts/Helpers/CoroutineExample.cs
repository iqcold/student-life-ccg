﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Scripts.Helpers.CoroutineWrapper
{
    public class CoroutineExample : CoroutineWrapper
    {
        protected override IEnumerator _coroutineMethod { get; set; }

        public static CoroutineExample Start(IEnumerator method)
        {
            CoroutineExample coroutine = new CoroutineExample(method);
            coroutine.Start();
            return coroutine;
        }

        protected CoroutineExample(IEnumerator method) : base(method) { }

        protected override void OnStart()
        {
            throw new System.NotImplementedException();
        }

        protected override void OnLateStart()
        {
            throw new System.NotImplementedException();
        }

        protected override void OnStop()
        {
            throw new System.NotImplementedException();
        }

        protected override void OnLateStop()
        {
            throw new System.NotImplementedException();
        }
    }
}