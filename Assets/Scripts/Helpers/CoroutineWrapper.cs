﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Scripts.Helpers.CoroutineWrapper
{
    public abstract class CoroutineWrapper : MonoBehaviour
    {
        protected Coroutine _coroutine;
        protected abstract IEnumerator _coroutineMethod { get; set; }

        protected CoroutineWrapper(IEnumerator method)
        {
            _coroutineMethod = method;
        }

        public void Start()
        {
            OnStart();
            _coroutine = StartCoroutine(_coroutineMethod);
            OnLateStart();
        }

        public void Stop()
        {
            OnStop();
            if (_coroutine != null)
            {
                StopCoroutine(_coroutine);
                _coroutine = null;
            }
            OnLateStop();
        }

        protected abstract void OnStart();
        protected abstract void OnLateStart();
        protected abstract void OnStop();
        protected abstract void OnLateStop();
    }
}