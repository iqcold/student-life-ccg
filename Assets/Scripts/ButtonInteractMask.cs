﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ButtonInteractMask : MonoBehaviour
{
    [Range(0F, 1F)]
    public float AlphaCutoff = 0.01F;

    void Start()
    {
        GetComponent<Image>().alphaHitTestMinimumThreshold = AlphaCutoff;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
