﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Scripts.Card;

namespace Scripts.Board
{
    public abstract class CardHolder : MonoBehaviour
    {
        public enum HolderTypes
        {
            PlayerHand,
            EnemyHand,
            PlayerField,
            EnemyField,
            Deck,
        }

        protected List<PlayableCard> _cards;
        public abstract HolderTypes HolderType { get; }

        public abstract void AddCard(PlayableCard card);

        public abstract void Sort();

        protected abstract void SetCardsPosition();

        protected abstract void SetCardsRotation();

    }
}