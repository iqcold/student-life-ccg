﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Scripts.Board
{
    public class PlayerHand : Hand
    {
        public override HolderTypes HolderType => CardHolder.HolderTypes.PlayerHand;

        protected override void Awake()
        {
            base.Awake();
        }
    }
}