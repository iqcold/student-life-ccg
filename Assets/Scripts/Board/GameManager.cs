﻿using System.Collections;
using System.Collections.Generic;
using Scripts.Card;
using UnityEngine;

namespace Scripts.Board
{
    public class GameManager : MonoBehaviour
    {
        public CardManager CardManager;

        public int MaxCardCountInHand => 20;

        void Awake()
        {
            CardManager = FindObjectOfType<CardManager>();
        }

        // Update is called once per frame
        void Update()
        {

        }
    }
}