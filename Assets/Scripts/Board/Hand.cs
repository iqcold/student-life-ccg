﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using Scripts.Card;

namespace Scripts.Board
{
    /// <summary>
    /// Якорь карты должен быть всегда в левом нижнем углу
    /// </summary>
    public abstract class Hand : CardHolder
    {

        public const float CardMaxRotation = 40F;
        public const float LocalCardHeightOnHover = 100F;
        public const float LocalCardDefaultDepth = -90F;
        protected static GameManager _gameManager;

        protected RectTransform _cardPrefab;
        protected Vector2 _cardMinAnchor;

        protected static readonly CostComparator _costComparator = new CostComparator();

        public override void AddCard(PlayableCard card)
        {
            card.DefaultCardHolder = this;
            _cards.Add(card);
            Sort();
        }

        /// <summary>
        /// Sorting by cost
        /// </summary>
        public override void Sort()
        {
            if (_cards.Count != 0)
            {
                _cards.Sort(_costComparator);
                //SetCardsRotation();
                SetCardsPosition();
            }
        }

        protected override void SetCardsPosition()
        {
            float handWidth = GetComponent<RectTransform>().rect.width -
                              _gameManager.CardManager.CardPrefab.GetComponent<RectTransform>().rect.width / 2;
            float cellWidth = handWidth / _cards.Count;
            for (int i = 0; i < _cards.Count; i++)
            {
                float cardHeight =
                    LocalCardHeightOnHover - LocalCardHeightOnHover / _cards.Count
                    * Mathf.Abs(-_cards.Count / 2 + i + (_cards.Count % 2 == 0 && i >= _cards.Count / 2 ? 1 : 0)) * 2;

                _cards[i].SetLocalPosition(new Vector3(
                    _cardMinAnchor.x + (-handWidth / 2 + cellWidth * i + cellWidth / 2),
                    _cardMinAnchor.y + cardHeight,
                    LocalCardDefaultDepth - (0.00001F * (i + 1))
                ));
            }
        }

        protected override void SetCardsRotation()
        {
            foreach (PlayableCard card in _cards)
            {
                RectTransform transform = card.GetComponent<RectTransform>();
                transform.SetPositionAndRotation(transform.position, GetComponent<RectTransform>().rotation);
            }

            if (_cards.Count > 3)
            {
                for (int i = 0; i < _cards.Count; i++)
                {
                    // Transform transform = _cards[i].GetComponent<Transform>();
                    // float rotation = -CardMaxRotation / _gameManager.MaxCardCountInHand
                    //                  * (-_cards.Count / 2 + i +
                    //                     (_cards.Count % 2 == 0 && i >= _cards.Count / 2 ? 1 : 0)) * 2;
                    // transform.Rotate(0F, 0F, rotation);
                }
            }
        }

        protected virtual void Awake()
        {
            _gameManager = FindObjectOfType<GameManager>();
            _cards = new List<PlayableCard>();
            _cardMinAnchor = _gameManager.CardManager.CardPrefab.GetComponent<RectTransform>().anchorMin;
            _cardPrefab = _gameManager.CardManager.CardPrefab.GetComponent<RectTransform>();
        }

        protected sealed class CostComparator : IComparer<PlayableCard>
        {
            public int Compare(PlayableCard x, PlayableCard y)
            {
                int cost1 = x.CardInfo.Cost;
                int cost2 = x.CardInfo.Cost;

                if (cost1 == cost2)
                {
                    return 0;
                }

                if (cost1 > cost2)
                {
                    return 1;
                }

                return -1;
            }
        }
    }
}